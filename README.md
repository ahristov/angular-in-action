# Angular in Action

Contains notes reading the book ["Angular in Action" from Manning](https://www.manning.com/books/angular-in-action)

## First Project

### First Project: Service

_Services_ are objects that abstract common logic to reuse.

![Component](./doc/img/FirstProj_Service-01.png)

To generate the first service class, run:

```bash
ng generate service services/stocks
```

, which generates file `src/app/services/stocks.service.ts`:

```js
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const stocks: Array<string> = ['AAPL', 'GOOG', 'FB', 'AMZN', 'TWTR'];
const service = 'https://angular2-in-action-api.herokuapp.com';

export interface StockInterface {
  symbol: string;
  lastTradePriceOnly: number;
  change: number;
  changeInPercent: number;
}

@Injectable()
export class StocksService {

  constructor(private http: HttpClient) { }

  get() {
    // .slice() always returns a copy of the array.
    return stocks.slice();
  }

  add(stock) {
    stocks.push(stock);
    return this.get();
  }

  remove(stock) {
    stocks.splice(stocks.indexOf(stock), 1);
    return this.get();
  }

  load(symbols) {
    if (symbols) {
      return this.http.get<Array<StockInterface>>(service + '/stocks/snapshot?symbols=' + symbols.join());
    }
  }
}
```

After importing its dependencies, it defines and exports the `StockInterface` for other components to use. This provides for TypeScript definition of what a stock object is.

The `StocksService` class is then defined and exported and decorated by the `Injectable` decorator. This will let Angular to set up proper wiring of the class elsewhere.

In the constructor method, the is injected HttpClient service is injected using the TypeScript technique of declaring a private variable called `http` giving it then a type of HttpClient.

The `get()` method returns a copy of the `stocks` array, using `.slice()` method.

The `add()` method adds a new item to the `stocks` array.

The `remove()` method will drop an item from the `stocks` array.

The `load()` method makes a call to the HttpClient service to load the data for current stock price values. The HttpClient service is called and returns an **observable**, which is a construct for handling asynchronous events, such as data from an API call.

The `http.get()` method has a feature called _type variable_, a TypeScript feature that allows us to tell the method what type of object it should expect, an array of StockInterface in this case:

```js
this.http.get<Array<StockInterface>>(...
```

After the service is declared, it needs to be registered into the App module. In the `src/app/app.module.ts` we add these two imports at the top:

```js
import { HttpClientModule } from '@angular/common/http';
import { StocksService } from './services/stocks.service';
```

This imports those into the file, but we need to register the `HttpClientModule` with the application:

```js
@NgModule({
...
  imports: [
    BrowserModule,
    HttpClientModule
  ],
```

, and we need to register the new `StocksService` with the `providers` property to inform Angular that it should be made available for the module to use:

```js
@NgModule({
...
  providers: [StocksService],
```

### First Project: Component

![Component](./doc/img/FirstProj_ComponentPlacement-01.png)

![Component](./doc/img/FirstProj_Component-01.png)

To generate the first component, run:

```bash
ng generate component components/summary
```

, which generates a component composed of 4 files in directory `src/app/components/summary`:

```js
summary.component.css
summary.component.html
summary.component.spec.ts
summary.component.ts
```

It contains an empty CSS file, basic HTML template, test stub, and
empty class already initialized with the component annotation.

Directive attribute:

```js
[ngClass]="{increase: isPositive(), decrease: isNegative()}"
```

_Directives_ allow to modify the behavior and display of the DOM elements in a template. They can possibly add some conditional logic to modify how the template behaves or renders.

The NgClass directive is able to add or remove CSS classes. It is assigned an object with properties that are CSS class names. These properties map to a method on the controller. If the method returns true, it will add the class to the template. If the method returns false, it will remove the class from the template.

Interpolation and Angular expressions:

```js
{{stock?.symbol?.toUpperCase()}}
```

The double curly braces syntax is called _interpolation_. It is a way to display something on the page. The content between the curly braces is called _Angular expression_ and it is evaluated against the controller just like the directive. It this case it will display a property. If it fails, it will throw an error, but in this case the _safe navigation operator_ `?.` will silently fail and not display anything if the property is missing.

Pipes:

```js
{{stock?.lastTradePriceOnly | currency:'USD':'symbol':'.2'}} ({{stock?.changeInPercent | percent:'.2'}})
```

The _pipes_ are added to the Angular expression to format the output using the symbol `|`. In the above expression the _Currency pipe_ will convert the value of the float number (like 111.8) formatted as USD price (like $111.80). The _Percentage pipe_ will take as a decimal (like 0.06), and turn it into percentage (like 6%).

The template requires a controller to wire up the data and the methods, which is in this case the file `src/app/components/summary/summary.component.ts`:

![Controller 1/2](./doc/img/FirstProj_ComponentController1-01.png)

![Controller 2/2](./doc/img/FirstProj_ComponentController2-01.png)

For the CCC, this is the content of the file `src/app/components/summary/sum-
mary.component.css`:

```css
:host .stock-card {
  background: #333333;
  color: #fff;
}

:host .stock-card.increase {
  background: #558B2f;
}

:host .stock-card.decrease {
  background: #c62828;
}
```

The interesting part is the `:host` selector. Because components need to be as self-contained as possible, they rely on the Shadow DOM concepts. The host selector is a way to specify that you want the styles to apply to the element that hosts the element.

## Angular Essentials

### Template expressions

![Template bindings](./doc/img/Essentials_Template_Bindings_01.png)

#### Interpolation

Interpolations always use the `{{expression}}` syntax to bind data into the template.

Examples:

```js
<!-- 1. Calculates the value of two numbers, adds to 30 -->
{{10 + 20}}
<!-- 2. Outputs a string "Just a simple string" -->
{{'Just a simple string'}}
<!-- 3. Binds into an attribute value, to link to profile -->
<a href="/users/{{user.user_id}}">View Profile</a>
<!-- 4. Outputs first and last name -->
{{user.first_name}} {{user.last_name}}
<!-- 5. Calls a method in the controller that should return a string -->
{{getName()}}
```

#### Property binding

Bind values to properties, such as `class`, `disabled`, `href`, or `textContent`, like:

```js
<img [src]="user.img" />
```

In fact, interpolation is shorthand for binding to the `textContent`:

```js
<p [textContent]="user.name"></p>
```

Using the `[]` syntax **binds to an element’s property**, not the attribute. This is an
important distinction, because properties are the DOM element’s property. That makes
it possible to use any valid HTML element property (such as the img src property).
Instead of binding the data to the attribute, you’re binding data directly to the element
property, which is quite efficient.

#### Special property bindings

They are a couple of special property bindings for setting a class and style property for an element.

The `class` property on an element is a `DOMTokenList` array. You can do:

```js
<div class="mdl-card stock-card mdl-shadow--2dp"
  [class.increase]="isPositive()"
  [class.decrease]="isNegative()" />
```

instead of:

```js
<div class="mdl-card stock-card mdl-shadow--2dp"
  [ngClass]="{increase1: isPositive(), decrease1: isNegative()}" />
```

Likewise the `style` propetrty is a `CSSStyleDeclarator`object, that holds all the CSS properties.

```js
<h1 [style.color]="getColor()">Title</h1>
```

Usually `NgClass` or `NgStyle` should be used, these are more for edge cases.

#### Event binding

The syntax for event bindings uses parentheses `()` to bind to a known event. You will
use the name of the event inside the parentheses, without the on part of the name. For
an example form submit event, you would write it like this:

```js
<form style="margin-bottom: 5px;" (submit)="add()">
  <input name="stock" [(ngModel)]="stock" class="mdl-textfield__input"
    type="text" placeholder="Add Stock" />
</form>
```

Two-way binding:

It uses both the property and event binding syntax together `[()]` (called _banana in a box_). It does a regular property binding and setting up an event binding for you behind the scenes.
